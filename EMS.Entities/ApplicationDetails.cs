﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Entities
{
    public class ApplicationDetails : Common
    {
        public DateTime Date { get; set; }
        [Column(TypeName = "text")]
        public string Application { get; set; }
        public string Status { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        [NotMapped]
        public string EmpId { get; set; }
    }
}
