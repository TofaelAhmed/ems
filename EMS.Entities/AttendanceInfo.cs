﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Entities
{
    public class AttendanceInfo
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public int EmployeeId { get; set; }
        public string TimeInTimeOut { get; set; }
        public virtual Employee Employee { get; set; }
        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string EmpId { get; set; }
        [NotMapped]
        public string AttDate { get; set; }
        [NotMapped]
        //public string Name { get; set; }
        public string TimeIn { get; set; }
        [NotMapped]
        public string TimeOut { get; set; }
        [NotMapped]
        public double WorkingMin { get; set; }
        [NotMapped]
        public string Designation { get; set; }
        [NotMapped]
        public decimal Rate { get; set; }
        [NotMapped]
        public decimal Deduction { get; set; }
        [NotMapped]
        public decimal AdvanceCash { get; set; }
        [NotMapped]
        public decimal BasicSalary { get; set; }
    }
}
