﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Entities.ViewModel
{
    public class ViewModelAttandanceInfo
    {
        public int id { get; set; }
        public int EmployeeId { get; set; }
        public string AttDate { get; set; }
        //public string Name { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public double WorkingMin{ get; set; }
        public virtual Employee Employee { get; set; }
    }
}
