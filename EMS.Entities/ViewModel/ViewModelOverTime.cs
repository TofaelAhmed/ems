﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Entities.ViewModel
{
    public class ViewModelOverTime
    {
        public string Date { get; set; }
        public string EmpId { get; set; }
        public string Name { get; set; }
        public string OverTime { get; set; }
        public string Rate { get; set; }
    }
}
