﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Entities.ViewModel
{
    public class ViewModelEmployeeDashboard
    {
        public List<ApplicationDetails> ApplicationsDetails { get; set; }
        public List<AttendanceInfo> AttendanceInfos { get; set; }
    }
}
