﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Entities
{
    public class Designation : Common
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public decimal BasicSalary { get; set; }
        [Required]
        public decimal DailyAllowance { get; set; }
        [Required]
        public decimal MedicalAllowance { get; set; }
        [Required]
        public decimal OverTimeRate { get; set; }
    }
}
