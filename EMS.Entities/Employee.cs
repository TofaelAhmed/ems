﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EMS.Entities
{
    public class Employee : Common
    {
        [Required]
        public string EmployeeId { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required]
        public DateTime Dob { get; set; }
        [Required]
        public string ContactNoOne { get; set; }
        public string ContactNoTwo { get; set; }
        [Required]
        public string Email { get; set; } 
        public string Gender { get; set; }
        public string Password { get; set; }
        [Required]
        public string PresentAddress { get; set; }
        [Required]
        public string PermanentAddress { get; set; }
        public string PicUrl { get; set; }
        [Required]
        public DateTime JoinDate { get; set; }
        public decimal BasicSalary { get; set; }
        [Required]
        public int DesignationId { get; set; }
        public virtual Designation Designation { get; set; }
        [NotMapped]
        public HttpPostedFileBase ImageUpload { get; set; }
    }
}
