﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Entities
{
    public class Deduction : Common
    {
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public string Purpose { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        [NotMapped]
        public string EmpId { get; set; }
    }
}
