﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Entities
{
    public class Salary : Common
    {
        public Decimal AdvanceCash { get; set; }
        public Decimal Deduction { get; set; }
        public Decimal OverTime { get; set; }
        public Decimal Bonus { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        [NotMapped]
        public string EmpId { get; set; }
    }
}
