﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Entities
{
    public class Role : Common
    {
        public string Name { get; set; }
        public string Details { get; set; }
    }
}
