﻿using EMS.Entities;
using EMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Data
{
    public partial class ProjectContext : DbContext
    {
        public ProjectContext() : base("ProjectContext")
        {

        }
        //public static ProjectContext Create()
        //{
        //    return new ProjectContext();
        //}
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }

        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Designation> Designations { get; set; }
        public virtual DbSet<Deduction> Deductions { get; set; }
        public virtual DbSet<AdvancePayment> AdvancePayments { get; set; }
        public virtual DbSet<AttendanceInfo> AttendanceInfos { get; set; }
        public virtual DbSet<ApplicationDetails> ApplicationDetails { get; set; }
        public virtual DbSet<Salary> Salaries { get; set; }
        //public virtual DbSet<ViewModelAttandanceInfo> ViewModelAttandanceInfos { get; set; }
    }
}
