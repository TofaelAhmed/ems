namespace EMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deleteVW : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ViewModelAttandanceInfoes", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.ViewModelAttandanceInfoes", new[] { "EmployeeId" });
            DropTable("dbo.ViewModelAttandanceInfoes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ViewModelAttandanceInfoes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        AttDate = c.String(),
                        TimeIn = c.String(),
                        TimeOut = c.String(),
                        WorkingMin = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateIndex("dbo.ViewModelAttandanceInfoes", "EmployeeId");
            AddForeignKey("dbo.ViewModelAttandanceInfoes", "EmployeeId", "dbo.Employees", "Id", cascadeDelete: true);
        }
    }
}
