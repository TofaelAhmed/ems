﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Security.Model
{
    public class LoginDetails
    {
        public string AdminId { get; set; }  
        public string Password { get; set; }
        public bool RememberMe { get; set; }

    }
}
