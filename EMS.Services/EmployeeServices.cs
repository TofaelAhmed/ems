﻿using EMS.Data;
using EMS.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services
{
    public class EmployeeServices
    {
        ProjectContext db = new ProjectContext();

        public List<Employee> GetAll()
        {
            return db.Employees.Where(x => x.IsDelete == false).ToList();
        }
        public int CountTotalEmp()
        {
            return db.Employees.ToList().Count();
        }
        public void Save(Employee emp)
        {
            emp.CreateDate = DateTime.Now;
            emp.IsDelete = false;
            db.Employees.Add(emp);
            db.SaveChanges();
        }
        public Employee GetEmployeeById(int id)
        {
            return db.Employees.Where(x => x.Id == id).SingleOrDefault();
        }
        public Employee GetEmployeeByEmployeeId(string empId)
        {
            return db.Employees.Where(x => x.EmployeeId == empId).SingleOrDefault();
        }

        public Employee GetEmployeeByIdPassword(string empId, string password)
        {
            return db.Employees.Where(x => x.EmployeeId == empId
           && x.Password == password && x.IsDelete == false).SingleOrDefault();
        }

        public void Update(Employee emp)
        {
            emp.UpdateDate = DateTime.Now;
            db.Entry(emp).State = EntityState.Modified;
            db.SaveChanges();
        }
        public void ChangePassword(Employee emp)
        {
           // emp.UpdateDate = DateTime.Now;
            var empInfo = db.Employees.Where(x => x.Id == emp.Id).SingleOrDefault();
            empInfo.Password = emp.Password;
            db.SaveChanges();
        }
        public void Delete(int id)
        {
            Employee emp = db.Employees.Where(x => x.Id == id).SingleOrDefault();
            emp.IsDelete = true;
            db.Entry(emp).State = EntityState.Modified;
            db.SaveChanges();
        }
        public string GetEmployeeId()
        {
            int empLastId = 0;
            var emp = db.Employees.OrderByDescending(a => a.Id).FirstOrDefault();
            if (emp != null)
            {
                empLastId = db.Employees.Max(x => x.Id);
            }
            return "Emp-" + (empLastId + 1);
        }

        public void SendLeaveApplication(ApplicationDetails details)
        {
            details.CreateDate = DateTime.Now;
            details.IsDelete = false;
            details.Status = "Pending";
            db.ApplicationDetails.Add(details);
            db.SaveChanges();
        }

        public void UpdateLeaveApplication(ApplicationDetails details)
        {
            throw new NotImplementedException();
        }

        public List<Employee> GetDesignationById()
        {
            var currentDateTime = DateTime.Now;
            var data = db.Employees.Where(x => x.JoinDate.Month == currentDateTime.Month && x.JoinDate.Year == currentDateTime.Year).ToList();
            return data;
        }
    }
}
