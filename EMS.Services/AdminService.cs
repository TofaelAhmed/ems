﻿using EMS.Data;
using EMS.Entities;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;

namespace EMS.Services
{
    public class AdminService
    {
        ProjectContext db = new ProjectContext();

        public List<Admin> GetAll()
        {
            return db.Admins.Where(x => x.IsDelete == false).ToList();
        }
        public void Save(Admin admin)
        {
            admin.CreateDate = DateTime.Now;
            admin.IsDelete = false;
            db.Admins.Add(admin);
            db.SaveChanges();
        }
        public Admin GetAdminByIdPassword(string adminId, string password)
        {
            return db.Admins.Where(x => x.AdminId == adminId
            && x.Password == password && x.IsDelete==false).SingleOrDefault();
        }
        public Admin GetAdminById(int id)
        {
            return db.Admins.Where(x => x.Id == id).SingleOrDefault();
        }
        public Admin GetAdminByAdminId(string adminId)
        {
            return db.Admins.Where(x => x.AdminId == adminId).SingleOrDefault();
        }
        public void Update(Admin admin)
        {
            db.Entry(admin).State = EntityState.Modified;
            db.SaveChanges();
        }
        public void Delete(int id)
        {
            Admin admin = db.Admins.Where(x => x.Id == id).SingleOrDefault();
            admin.IsDelete = true;
            db.Entry(admin).State = EntityState.Modified;
            db.SaveChanges();
        }
        public string GetAdminId()
        {
            int adminLastId = 0;
            var admin = db.Admins.OrderByDescending(a => a.Id).FirstOrDefault();
            if (admin != null)
            {
                adminLastId = db.Admins.Max(x => x.Id);
            }
            return "Adm-" + (adminLastId + 1);
        }
        public Admin GetRoleByName(string adminId, string password)
        {
            return db.Admins.Where(x => x.AdminId == adminId
            && x.Password == password).SingleOrDefault();
        }

    }
}
