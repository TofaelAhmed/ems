﻿using EMS.Data;
using EMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services
{
    public class RoleService
    {
        ProjectContext db = new ProjectContext();
        public List<Role> GetAll()
        {
            return db.Roles.ToList();
        }
        public string[] GetRoleByName(string id)
        {
            string roleName = string.Empty;
            var data = db.Admins.Where(x => x.AdminId == id).FirstOrDefault();
            if(data == null)
            {
                roleName = "Employee";
            }
            else
            {
                roleName = data.Role.Name;
            }
            string[] role = { roleName };
            return role;
        }
    }
}
