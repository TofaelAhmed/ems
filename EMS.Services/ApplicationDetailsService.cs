﻿using EMS.Data;
using EMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services
{
    public class ApplicationDetailsService
    {
        ProjectContext db = new ProjectContext();

        public List<ApplicationDetails> GetAll()
        {
            return db.ApplicationDetails.Where(x => x.IsDelete == false).ToList();
        }
        public List<ApplicationDetails> GetApplicationByEmpId(string empId)
        {
            return db.ApplicationDetails.Where(x => x.Employee.EmployeeId == empId).ToList();
        }
        public ApplicationDetails GetApplicationDetailsById(int id)
        {
            return db.ApplicationDetails.Where(x => x.Id == id).SingleOrDefault();
        }

        public List<ApplicationDetails> GetApplicationByPendingStatus()
        {
            return db.ApplicationDetails.Where(x => x.Status =="Pending").ToList();
        }
        public void ChangeApplication(int id,string status)
        {
            var app = db.ApplicationDetails.Where(x => x.Id == id).SingleOrDefault();
            app.Status = status;
            db.SaveChanges();
        }
    }
}
