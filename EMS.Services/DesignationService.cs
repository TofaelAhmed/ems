﻿using EMS.Data;
using EMS.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services
{
    public class DesignationService
    {
        ProjectContext db = new ProjectContext();

        public List<Designation> GetAll()
        {
            return db.Designations.Where(x => x.IsDelete == false).ToList();
        }
        public void Save(Designation designation)
        {
            designation.CreateDate = DateTime.Now;
            designation.IsDelete = false;
            db.Designations.Add(designation);
            db.SaveChanges();
        }
        
        public Designation GetDesignationById(int id)
        {
            return db.Designations.Where(x => x.Id == id).SingleOrDefault();
        }
        public void Update(Designation designation)
        {
            db.Entry(designation).State = EntityState.Modified;
            db.SaveChanges();
        }
        public void Delete(int id)
        {
            Designation designation = db.Designations.Where(x => x.Id == id).SingleOrDefault();
            designation.IsDelete = true;
            db.Entry(designation).State = EntityState.Modified;
            db.SaveChanges();
        }

        public object GetAllRecntJoinEmp()
        {
            throw new NotImplementedException();
        }
    }
}
