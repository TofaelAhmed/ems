﻿using EMS.Data;
using EMS.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services
{
    public class DeductionService
    {
        ProjectContext db = new ProjectContext();

        public List<Deduction> GetAll()
        {
            return db.Deductions.Where(x => x.IsDelete == false).ToList();
        }
        public void Save(Deduction deduction)
        {
            deduction.CreateDate = DateTime.Now;
            deduction.IsDelete = false;
            db.Deductions.Add(deduction);
            db.SaveChanges();
        }

        public Deduction GetDeductionById(int id)
        {
            return db.Deductions.Where(x => x.Id == id).SingleOrDefault();
        }
        public void Update(Deduction deduction)
        {
            db.Entry(deduction).State = EntityState.Modified;
            db.SaveChanges();
        }
        public void Delete(int id)
        {
            Deduction deduction = db.Deductions.Where(x => x.Id == id).SingleOrDefault();
            deduction.IsDelete = true;
            db.Entry(deduction).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
