﻿using EMS.Data;
using EMS.Entities;
using EMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services
{
    public class AttandanceService
    {
        ProjectContext db = new ProjectContext();

        public List<AttendanceInfo> GetAll()
        {
            return db.AttendanceInfos.ToList();
        }
        public List<AttendanceInfo> CurrentMonthAttendanceReport()
        {
            var sorted = db.AttendanceInfos.OrderBy(d => d.EmployeeId).ThenBy(d => d.DateTime).ToList();
            var zipped = sorted.Where(d => d.TimeInTimeOut == "Time In").Zip(sorted.Where(s => s.TimeInTimeOut == "Time Out"),
                            (i, o) =>
                            {
                                //Debug.Assert(i.EmployeeId == o.EmployeeId);
                                return new AttendanceInfo
                                {
                                    AttDate = i.DateTime.ToString("d"),
                                    EmpId = i.Employee.EmployeeId,
                                    Name = i.Employee.FirstName + " " + i.Employee.LastName,
                                    TimeIn = i.DateTime.ToString(),
                                    TimeOut = o.DateTime.ToString(),
                                    WorkingMin = (o.DateTime - i.DateTime).TotalMinutes
                                };
                            }).OrderBy(d => d.TimeIn);
            var currentDateTime = DateTime.Now.Date;
            List<AttendanceInfo> currentAtt = new List<AttendanceInfo>();
            foreach (var d in zipped)
            {
                DateTime punchDateTime = Convert.ToDateTime(d.AttDate);
                if (punchDateTime.Month == currentDateTime.Month && punchDateTime.Year == currentDateTime.Date.Year)
                {
                    currentAtt.Add(d);
                }
            }
            return currentAtt;
        }
        public List<AttendanceInfo> CurrentMonthOverTimeReport()
        {
            var sorted = db.AttendanceInfos.OrderBy(d => d.EmployeeId).ThenBy(d => d.DateTime).ToList();
            var zipped = sorted.Where(d => d.TimeInTimeOut == "Time In").Zip(sorted.Where(s => s.TimeInTimeOut == "Time Out"),
                            (i, o) =>
                            {
                                //Debug.Assert(i.EmployeeId == o.EmployeeId);
                                return new AttendanceInfo
                                {
                                    AttDate = i.DateTime.ToString("d"),
                                    EmpId = i.Employee.EmployeeId,
                                    Name = i.Employee.FirstName + " " + i.Employee.LastName,
                                    Designation = i.Employee.Designation.Title,
                                    WorkingMin = (o.DateTime - i.DateTime).Hours,
                                    Rate = i.Employee.Designation.OverTimeRate
                                };
                            }).OrderBy(d => d.TimeIn);
            var currentDateTime = DateTime.Now.Date;
            List<Employee> emp = db.Employees.ToList();
            List<AttendanceInfo> currentAtt = new List<AttendanceInfo>();
            var duplicateCheck = currentAtt.Select(x => x.AttDate).Distinct().ToList();


            //var distinctedList = zipped.DistinctBy(x => x.).ToList();
            foreach (var d in zipped)
            {
                DateTime punchDateTime = Convert.ToDateTime(d.AttDate);
                if (punchDateTime.Month == currentDateTime.Month && punchDateTime.Year == currentDateTime.Date.Year)
                {

                    if (currentAtt.Count > 0)
                    {
                        var att = currentAtt.Find(c => (c.AttDate == d.AttDate) && (c.EmpId == d.EmpId));
                        if (att != null)
                        {
                            currentAtt.Remove(att);
                            att.WorkingMin = att.WorkingMin + d.WorkingMin;
                            currentAtt.Add(att);
                        }
                        else
                        {
                            currentAtt.Add(d);
                        }

                    }
                    else
                    {
                        currentAtt.Add(d);
                    }

                }
            }
            List<AttendanceInfo> currentAttOvertime = new List<AttendanceInfo>();
            foreach (var da in currentAtt)
            {

                if (da.WorkingMin > 8)
                {
                    da.WorkingMin = da.WorkingMin - 8;
                    currentAttOvertime.Add(da);
                }

            }
            return currentAttOvertime;
        }
        public List<AttendanceInfo> CurrentMonthSalaryReport()
        {
            var sorted = db.AttendanceInfos.OrderBy(d => d.EmployeeId).ThenBy(d => d.DateTime).ToList();
            var zipped = sorted.Where(d => d.TimeInTimeOut == "Time In").Zip(sorted.Where(s => s.TimeInTimeOut == "Time Out"),
                            (i, o) =>
                            {
                                //Debug.Assert(i.EmployeeId == o.EmployeeId);
                                return new AttendanceInfo
                                {
                                    AttDate = i.DateTime.ToString("d"),
                                    EmpId = i.Employee.EmployeeId,
                                    Name = i.Employee.FirstName + " " + i.Employee.LastName,
                                    Designation = i.Employee.Designation.Title,
                                    WorkingMin = (o.DateTime - i.DateTime).Hours,
                                    BasicSalary = i.Employee.Designation.BasicSalary + i.Employee.Designation.MedicalAllowance + i.Employee.Designation.DailyAllowance,
                                    Rate = i.Employee.Designation.OverTimeRate,
                                };
                            }).OrderBy(d => d.TimeIn);
            var currentDateTime = DateTime.Now.Date;
            List<Employee> emp = db.Employees.ToList();
            List<AttendanceInfo> currentAtt = new List<AttendanceInfo>();
            var duplicateCheck = currentAtt.Select(x => x.AttDate).Distinct().ToList();


            //var distinctedList = zipped.DistinctBy(x => x.).ToList();
            foreach (var d in zipped)
            {
                DateTime punchDateTime = Convert.ToDateTime(d.AttDate);
                if (punchDateTime.Month == currentDateTime.Month && punchDateTime.Year == currentDateTime.Date.Year)
                {

                    if (currentAtt.Count > 0)
                    {
                        var att = currentAtt.Find(c => (c.AttDate == d.AttDate) && (c.EmpId == d.EmpId));
                        if (att != null)
                        {
                            currentAtt.Remove(att);
                            att.WorkingMin = att.WorkingMin + d.WorkingMin;
                            currentAtt.Add(att);
                        }
                        else
                        {
                            currentAtt.Add(d);
                        }

                    }
                    else
                    {
                        currentAtt.Add(d);
                    }

                }
            }
            List<AttendanceInfo> currentAttOvertime = new List<AttendanceInfo>();
            foreach (var da in currentAtt)
            {

                if (da.WorkingMin > 8)
                {
                    da.WorkingMin = da.WorkingMin - 8;
                    currentAttOvertime.Add(da);
                }

            }
            List<AttendanceInfo> currentSalary = new List<AttendanceInfo>();
            List<AdvancePayment> advance = db.AdvancePayments.Where(x => x.IsDelete == false).ToList();
            List<Deduction> deductions = db.Deductions.Where(x => x.IsDelete == false).ToList();
            foreach (var d in currentAttOvertime)
            {
                if (currentAtt.Count > 0)
                {
                    var att = currentAtt.Find(c => (c.AttDate == d.AttDate) && (c.EmpId == d.EmpId));
                    if (att != null)
                    {
                        currentAtt.Remove(att);
                        decimal overtime = Convert.ToDecimal(d.WorkingMin) * att.Rate;
                        att.BasicSalary = att.BasicSalary + overtime;
                        currentAtt.Add(att);
                    }
                }
            }
            //var data = currentAtt.Where(x => x.EmpId==x.EmpId).ToList();
            return currentAtt;
        }
        public int TodayAttendantedEmp()
        {
            var currentDateTime = DateTime.Now.Date;
            List<AttendanceInfo> data = db.AttendanceInfos.ToList();
            List<AttendanceInfo> todayAtt = new List<AttendanceInfo>();
            foreach (var d in data)
            {
                if (d.DateTime.Date == currentDateTime)
                {
                    todayAtt.Add(d);
                }
            }
            var duplicateCheck = todayAtt.Select(x => x.EmployeeId).Distinct();

            return duplicateCheck.Count();
        }
        public void Save(AttendanceInfo attendance)
        {
            db.AttendanceInfos.Add(attendance);
            db.SaveChanges();
        }
        public List<AttendanceInfo> GetAttandanceByEmpId(string emp)
        {
            return db.AttendanceInfos.Where(x => x.Employee.EmployeeId == emp).ToList();
        }
        public void Update(AttendanceInfo attendance)
        {
            db.Entry(attendance).State = EntityState.Modified;
            db.SaveChanges();
        }

        public double CountTodayWoringTime()
        {
            var att = CurrentMonthAttendanceReport();
            var currentDay = DateTime.Now.Date;
            var todayWorkingTime = att.Where(x => x.AttDate == currentDay.ToString("d")).Sum(x=>x.WorkingMin);
                //att.ToList().Where(a => DateTime.ParseExact(a.AttDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).Day == currentDay).Sum(x=>x.WorkingMin);
            return (todayWorkingTime / 60);
        }
        public double CountTodayLostTime()
        {
            double dailyWorkingTime = (TodayAttendantedEmp() * 8);
            return dailyWorkingTime - CountTodayWoringTime();
        }
    }
}
