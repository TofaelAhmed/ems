﻿using EMS.Data;
using EMS.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services
{
    public class AdvancePaymentService
    {
        ProjectContext db = new ProjectContext();

        public List<AdvancePayment> GetAll()
        {
            return db.AdvancePayments.Where(x => x.IsDelete == false).ToList();
        }
        public void Save(AdvancePayment advancePayment)
        {
            advancePayment.CreateDate = DateTime.Now;
            advancePayment.IsDelete = false;
            db.AdvancePayments.Add(advancePayment);
            db.SaveChanges();
        }

        public AdvancePayment GetAdvancePaymentById(int id)
        {
            return db.AdvancePayments.Where(x => x.Id == id).SingleOrDefault();
        }
        
        public void Update(AdvancePayment advancePayment)
        {
            db.Entry(advancePayment).State = EntityState.Modified;
            db.SaveChanges();
        }
        public void Delete(int id)
        {
            AdvancePayment advancePayment = db.AdvancePayments.Where(x => x.Id == id).SingleOrDefault();
            advancePayment.IsDelete = true;
            db.Entry(advancePayment).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
