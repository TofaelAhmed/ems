﻿using EMS.Entities;
using EMS.Security;
using EMS.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMS.Web.Controllers
{
    [Authorize(Roles ="Admin,Super Admin")]
    public class AttendanceController : Controller
    {
        AttandanceService service = new AttandanceService();
        public ActionResult GetAttendanceInfo()
        {
            return View();
        }
        public JsonResult SaveOrUpdateAttendnce(AttendanceInfo attendance)
        {
            LoginStatus status = new LoginStatus();
            status.Success = false;
            try
            {
                EmployeeServices employeeServices = new EmployeeServices();
                var emp = employeeServices.GetEmployeeByEmployeeId(attendance.EmpId);
                if (emp != null)
                {
                    attendance.EmployeeId = emp.Id;
                }
                else
                {
                    status.Message = "Invalid Employee Id";
                    ModelState.AddModelError("", "Invalid Employee Id");
                }

                if (ModelState.IsValid)
                {
                    if (attendance.Id <= 0)
                    {
                        service.Save(attendance);
                        status.Success = true;
                        status.Message = "Attendance Successfull!";
                    }
                    else
                    {
                        service.Update(attendance);
                        status.Success = true;
                        status.Message = "Successfully Update Attendance Info";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CurrentMonthAttendance()
        {
            var data = service.CurrentMonthAttendanceReport();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CurrentMonthOverTimeReport()
        {
            var data = service.CurrentMonthOverTimeReport();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOverTimeInfo()
        {
            return View();
        }
    }
}