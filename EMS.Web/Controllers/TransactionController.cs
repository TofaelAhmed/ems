﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMS.Services;
using EMS.Entities;
using EMS.Security;

namespace EMS.Web.Controllers
{
    [Authorize(Roles = "Admin,Super Admin")]
    public class TransactionController : Controller
    {
        // GET: Transaction
        public ActionResult GetSalaryInfo()
        {
            return View();
        }
        public JsonResult CurrentMonthSalaryReport()
        {
            AttandanceService attandanceService = new AttandanceService();
            var data = attandanceService.CurrentMonthSalaryReport();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #region Advance Payment 
        AdvancePaymentService service = new AdvancePaymentService();
        public ActionResult AdvancePayment()
        {
            return View();
        }
        public JsonResult GetAllAdvancePaymentInfo()
        {
            var allAdvancePaymentInfo = service.GetAll();
            return Json(allAdvancePaymentInfo, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveOrUpdateAdvancePayment(AdvancePayment advancePayment)
        {
            LoginStatus status = new LoginStatus();
            status.Success = false;
            try
            {
                EmployeeServices employeeServices = new EmployeeServices();
                var emp = employeeServices.GetEmployeeByEmployeeId(advancePayment.EmpId);
                if (emp != null)
                {
                    advancePayment.EmployeeId = emp.Id;
                }
                else
                {
                    status.Message = "Invalid Employee Id";
                    ModelState.AddModelError("", "Invalid Employee Id");
                }

                if (ModelState.IsValid)
                {
                    if (advancePayment.Id <= 0)
                    {
                        service.Save(advancePayment);
                        status.Success = true;
                        status.Message = "Successfully Saved New Deduction";
                    }
                    else
                    {
                        service.Update(advancePayment);
                        status.Success = true;
                        status.Message = "Successfully Update Deduction Info";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}