﻿using EMS.Entities;
using EMS.Entities.ViewModel;
using EMS.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMS.Web.Controllers
{

    public class AdminController : Controller
    {
        AdminService service = new AdminService();
        [Authorize(Roles = "Super Admin")]
        public ActionResult Admin()
        {
            RoleService roleService = new RoleService();
            ViewBag.Role = roleService.GetAll();
            return View();
        }
        [Authorize(Roles = "Super Admin")]
        public JsonResult GetAll()
        {
            var allAdminInfo = service.GetAll();
            return Json(allAdminInfo, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Super Admin")]
        public JsonResult SaveOrUpdate(Admin admin)
        {
            string response = "";
            try
            {
                if (ModelState.IsValid)
                {
                    if (admin.Id <= 0)
                    {
                        service.Save(admin);
                        response = "successfully Saved New Admin";
                    }
                    else
                    {
                        service.Update(admin);
                        response = "successfully Update Admin Info";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Super Admin")]
        public JsonResult GetAdminId()
        {
            return Json(service.GetAdminId(), JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Super Admin,Admin")]
        public ActionResult AdminProfile()
        {
            string adminId = User.Identity.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
            var data = service.GetAdminByAdminId(adminId);
            return View(data);
        }
        [Authorize(Roles = "Super Admin,Admin")]
        public ActionResult ReviewLeaveApplication()
        {
            ViewModelEmployeeDashboard model = new ViewModelEmployeeDashboard();
            ApplicationDetailsService service = new ApplicationDetailsService();
            //var empId = User.Identity.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
            model.ApplicationsDetails = service.GetApplicationByPendingStatus();
            return View(model);
        }
        public ActionResult ApplicationDetails(int id)
        {
            ApplicationDetailsService service = new ApplicationDetailsService();
            //var empId = User.Identity.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
            var data = service.GetApplicationDetailsById(id);
            return View(data);
        }
        public JsonResult ReviewLeaveApp(int id, string status)
        {
            var msg = string.Empty;
            if (status == "Accepted")
            {
                msg = "Leave Apllication Successfully Accepted";
            }
            else
            {
                msg = "Leave Apllication Successfully Rejeted";
            }
            ApplicationDetailsService service = new ApplicationDetailsService();
            //var empId = User.Identity.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
            service.ChangeApplication(id, status);
            return Json(msg, JsonRequestBehavior.AllowGet);
        }


    }
}