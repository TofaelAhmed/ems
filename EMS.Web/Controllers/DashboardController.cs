﻿using EMS.Entities.ViewModel;
using EMS.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMS.Web.Controllers
{
    
    public class DashboardController : Controller
    {
        [Authorize(Roles = "Super Admin,Admin")]
        public ActionResult AdminDashboard()
        {
            AttandanceService service = new AttandanceService();
            ViewBag.TodayAttendEmp = service.TodayAttendantedEmp();
            ViewBag.TodayWorkTime = service.CountTodayWoringTime();
            ViewBag.TodayLostTime = service.CountTodayLostTime();
            EmployeeServices employeeServices = new EmployeeServices();
            ViewBag.TotalEmp = employeeServices.CountTotalEmp();
            return View();
        }
        [Authorize(Roles = "Employee")]
        public ActionResult EmployeeDashboard()
        {
            ViewModelEmployeeDashboard model = new ViewModelEmployeeDashboard();
            ApplicationDetailsService service = new ApplicationDetailsService();
            var empId = User.Identity.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
            model.ApplicationsDetails = service.GetApplicationByEmpId(empId);
            return View(model);
        }
    }
}