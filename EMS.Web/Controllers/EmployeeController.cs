﻿using EMS.Entities;
using EMS.Entities.ViewModel;
using EMS.Services;
using EMS.Web.EmailSending;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMS.Web.Controllers
{

    public class EmployeeController : Controller
    {
        private DesignationService dService = new DesignationService();
        private EmployeeServices eService = new EmployeeServices();

        #region Employee
        [Authorize(Roles = "Admin,Super Admin")]
        public ActionResult Index()
        {
            ViewBag.Designation = dService.GetAll();
            ViewBag.TotalEmp = eService.CountTotalEmp();
            return View();
        }
        [Authorize(Roles = "Admin,Super Admin")]
        public JsonResult GetAllEmployee()
        {
            var allEmpInfo = eService.GetAll();
            return Json(allEmpInfo, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin,Super Admin,Employee")]
        [HttpPost]
        public JsonResult SaveOrUpdateEmpInfo(Employee emp)
        {
            string response = "";
            try
            {
                if (ModelState.IsValid)
                {
                    emp.PicUrl = "user.jpg";
                    if (emp.ImageUpload != null)
                    {
                        string fileName = Path.GetFileNameWithoutExtension(emp.ImageUpload.FileName);
                        string extension = Path.GetExtension(emp.ImageUpload.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssff") + extension;
                        emp.PicUrl = fileName;
                        emp.ImageUpload.SaveAs(Path.Combine(Server.MapPath("~/AppFile/Images"), fileName));
                    }
                    if (emp.Id <= 0)
                    {
                        eService.Save(emp);
                        response = "successfully Saved New Employee";
                        SendEmail email = new SendEmail();
                        email.EmailInitialize(emp.EmployeeId, emp.Email);
                    }
                    else
                    {
                        eService.Update(emp);
                        response = "successfully Update Employee Info";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin,Super Admin")]
        public JsonResult GetEmpId()
        {
            return Json(eService.GetEmployeeId(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Designation
        [Authorize(Roles = "Admin,Super Admin")]
        public ActionResult Designation()
        {
            return View();
        }
        [Authorize(Roles = "Admin,Super Admin")]
        public JsonResult GetAllDesignationInfo()
        {
            var allDesignationInfo = dService.GetAll();
            return Json(allDesignationInfo, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin,Super Admin")]
        public JsonResult SaveOrUpdateDesignation(Designation designation)
        {
            string response = "";
            try
            {
                if (ModelState.IsValid)
                {
                    if (designation.Id <= 0)
                    {
                        dService.Save(designation);
                        response = "Successfully Saved New Designation";
                    }
                    else
                    {
                        dService.Update(designation);
                        response = "Successfully Update Designation Info";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Employee LeaveApplication And Employee Profile
        [Authorize(Roles = "Employee")]
        public ActionResult LeaveApplication()
        {
            ViewModelEmployeeDashboard data = new ViewModelEmployeeDashboard();
            return View();
        }
        [Authorize(Roles = "Employee,Admin,SuperAdmin")]
        public ActionResult ApplicationDetails(int id)
        {
            ApplicationDetailsService service = new ApplicationDetailsService();
            //var empId = User.Identity.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
            var data = service.GetApplicationDetailsById(id);
            return View(data);
        }
        [Authorize(Roles = "Employee")]
        [HttpPost]
        public ActionResult SendApplication(ApplicationDetails details)
        {
            string msg = string.Empty;
            try
            {
                EmployeeServices employeeServices = new EmployeeServices();
                if (ModelState.IsValid)
                {
                    var empId = User.Identity.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
                    details.EmployeeId = employeeServices.GetEmployeeByEmployeeId(empId).Id;
                    if (details.Id <= 0)
                    {
                        employeeServices.SendLeaveApplication(details);
                        msg = "Successfully Send Leave Application";
                    }
                    else
                    {
                        employeeServices.UpdateLeaveApplication(details);
                        msg = "Successfully Update Leave Application Info";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Employee")]
        public ActionResult EmployeeProfile()
        {
            string employeeId = User.Identity.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
            var data = eService.GetEmployeeByEmployeeId(employeeId);
            return View(data);
        }
        public ActionResult ChangeEmployeePassword(string employeeId)
        {
            //string employeeId = User.Identity.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
            var data = eService.GetEmployeeByEmployeeId(employeeId);
            return View(data);
        }
        [HttpPost]
        public JsonResult ChangeEmployeePassword(Employee emp)
        {
            string response = "";

            eService.ChangePassword(emp);
            response = "successfully Change Password";

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult GetDesignationById(int deductionId)
        {
            var data = dService.GetDesignationById(deductionId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllRecntJoinEmp()
        {
            var data = eService.GetDesignationById();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}