﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMS.Entities;
using EMS.Security;
using EMS.Services;

namespace EMS.Web.Controllers
{
    [Authorize(Roles = "Admin,Super Admin")]
    public class DeductionController : Controller
    {
        DeductionService service = new DeductionService();
        public ActionResult Deduction()
        {
            return View();
        }
        public JsonResult GetAllDeductionInfo()
        {
            var allDeductionInfo = service.GetAll();
            return Json(allDeductionInfo, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveOrUpdateDeduction(Deduction deduction)
        {
            LoginStatus status = new LoginStatus();
            status.Success = false;
            try
            {
                EmployeeServices employeeServices = new EmployeeServices();
                var emp = employeeServices.GetEmployeeByEmployeeId(deduction.EmpId);
                if (emp != null)
                {
                    deduction.EmployeeId = emp.Id;
                }
                else
                {
                    status.Message = "Invalid Employee Id";
                    ModelState.AddModelError("", "Invalid Employee Id");
                }

                if (ModelState.IsValid)
                {
                    if (deduction.Id <= 0)
                    {
                        service.Save(deduction);
                        status.Success = true;
                        status.Message = "Successfully Saved New Deduction";
                    }
                    else
                    {
                        service.Update(deduction);
                        status.Success = true;
                        status.Message = "Successfully Update Deduction Info";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }
        
    }
}